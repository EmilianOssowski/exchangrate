using ExchangeRatesAPI;
using ExchangeRatesAPI.Database;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace ExchabgeRatesAPI.Test
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                // Build the service provider.
                var sp = services.BuildServiceProvider();
                using (var scope = sp.CreateScope())
                {
                    //delete postgre context 
                    var serviceDescriptor = services.FirstOrDefault(descriptor => descriptor.ServiceType == typeof(DbContextOptions<SQLDbContext>));
                    var res = services.Remove(serviceDescriptor);
                    
                    // Create a new service provider.
                    var serviceProvider = new ServiceCollection()
                    .AddEntityFrameworkInMemoryDatabase()
                    .BuildServiceProvider();

                    // Add a database context (AppDbContext) using an in-memory database for testing.
                    services.AddDbContext<SQLDbContext>(options =>
                    {
                        options.UseInMemoryDatabase("InMemoryAppDb");
                        options.UseInternalServiceProvider(serviceProvider);
                    });



                    // Create a scope to obtain a reference to the database contexts
                    //using (var scope = sp.CreateScope())
                    //{
                    var scopedServices = services.BuildServiceProvider();
                    var appDb = scopedServices.GetRequiredService<SQLDbContext>();

                    var logger = scopedServices.GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();

                    // Ensure the database is created.
                    //appDb.Database.EnsureCreated();

                    try
                    {
                        // Seed the database with some specific test data.
                        SeedData.PopulateTestData(appDb);
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, "An error occurred seeding the " +
                                            "database with test messages. Error: {ex.Message}");
                    }
                }
            });
        }
    }
}
