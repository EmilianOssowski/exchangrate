﻿using ExchangeRatesAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Xunit;

namespace ExchabgeRatesAPI.Test
{
    public class CurrencyReadServiceBenchmarkTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        public CurrencyReadServiceBenchmarkTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public void AverageResponseTime_100Requests()
        {
            var allResponseTimes = new List<(DateTime Start, DateTime End)>();

            for (var i = 0; i < 100; i++)
            {
                using (var client = new HttpClient())
                {
                    var start = DateTime.Now;
                    var response = _client.GetAsync("/api/CurrencyRead?currencyCodes[PLN]=EUR&startDate=2019-09-03&endDate=2019-09-03&apiKey=TestKeyForExchangeApi").Result;
                    var end = DateTime.Now;

                    allResponseTimes.Add((start, end));
                }
            }

            var expected = 80;
            var actual = (int)allResponseTimes.Select(r => (r.End - r.Start).TotalMilliseconds).Average();
            Assert.True(actual <= expected, $"Expected average response time of less than or equal to {expected} ms but was {actual} ms.");
        }

        [Fact]
        public void TheSameResponseTime_2Requests()
        {
            var allResponseTimes = new List<(DateTime Start, DateTime End)>();

            for (var i = 0; i < 2; i++)
            {
                using (var client = new HttpClient())
                {
                    var start = DateTime.Now;
                    var response = _client.GetAsync("/api/CurrencyRead?currencyCodes[PLN]=EUR&startDate=2019-09-03&endDate=2019-09-03&apiKey=TestKeyForExchangeApi").Result;
                    var end = DateTime.Now;

                    allResponseTimes.Add((start, end));
                }
            }

            var first = (int)allResponseTimes.Select(r => (r.End - r.Start).TotalMilliseconds).First();
            var second = (int)allResponseTimes.Select(r => (r.End - r.Start).TotalMilliseconds).ElementAt(1);
            Assert.True(second < first, $"Expected second response time of less than to {first} ms but was {second} ms.");
        }


        [Fact]
        public void AverageForYearRangeResponseTime_100Requests()
        {

            var dates = new List<DateTime>();

            for (int i = 0; i < 365; i++)
            {
                dates.Add(new DateTime(2017, 01, 01).AddDays(i));
            }

            var firstResponseTimes = new List<(DateTime Start, DateTime End)>();

            foreach (var date in dates)
            {
                using (var client = new HttpClient())
                {
                    var start = DateTime.Now;
                    var response = _client.GetAsync("/api/CurrencyRead?currencyCodes[PLN]=EUR&startDate=" + date.ToString("yyyy-MM-dd") + "&endDate=" + date.ToString("yyyy-MM-dd") + "&apiKey=TestKeyForExchangeApi").Result;
                    var end = DateTime.Now;

                    firstResponseTimes.Add((start, end));
                }
            }

            var secondResponseTimes = new List<(DateTime Start, DateTime End)>();

            foreach (var date in dates)
            {
                using (var client = new HttpClient())
                {
                    var start = DateTime.Now;
                    var response = _client.GetAsync("/api/CurrencyRead?currencyCodes[PLN]=EUR&startDate=" + date.ToString("yyyy-MM-dd") + "&endDate=" + date.ToString("yyyy-MM-dd") + "&apiKey=TestKeyForExchangeApi").Result;
                    var end = DateTime.Now;

                    secondResponseTimes.Add((start, end));
                }
            }


            var firstAverage = (int)firstResponseTimes.Select(r => (r.End - r.Start).TotalMilliseconds).Average();
            var secondAverage = (int)secondResponseTimes.Select(r => (r.End - r.Start).TotalMilliseconds).Average();
            Assert.True(secondAverage < firstAverage, $"Expected second response average time of less than to {firstAverage} ms but was {secondAverage} ms.");
        }
    }
}
