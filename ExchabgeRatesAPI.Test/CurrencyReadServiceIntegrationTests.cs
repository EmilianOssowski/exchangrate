﻿using ExchangeRatesAPI;
using ExchangeRatesAPI.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ExchabgeRatesAPI.Test
{
    public class CurrencyReadServiceIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        public CurrencyReadServiceIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }


        [Fact]
        public async Task CanGetCurrencyRead()
        {
            // The endpoint or route of the controller action.
            var httpResponse = await _client.GetAsync("/api/CurrencyRead?currencyCodes[PLN]=EUR&startDate=2019-09-03&endDate=2019-09-03&apiKey=TestKeyForExchangeApi");

            // Must be successful.
            httpResponse.EnsureSuccessStatusCode();

            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var currencyReads = JsonConvert.DeserializeObject<List<CurrencyRead>>(stringResponse);
            Assert.True(currencyReads.Count == 1);
            foreach(var cr in currencyReads)
            {
                Assert.True(cr.Rates.Count > 0);
            }
        }

        [Fact]
        public async Task CanGetCurrencyReadForMultipleCurrencyCodes()
        {
            // The endpoint or route of the controller action.
            var httpResponse = await _client.GetAsync("/api/CurrencyRead?currencyCodes[PLN]=EUR&currencyCodes[USD]=EUR&currencyCodes[GBP]=EUR&startDate=2019-09-03&endDate=2019-09-03&apiKey=TestKeyForExchangeApi");

            // Must be successful.
            httpResponse.EnsureSuccessStatusCode();

            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var currencyReads = JsonConvert.DeserializeObject<List<CurrencyRead>>(stringResponse);
            Assert.True(currencyReads.Count == 3);
            foreach (var cr in currencyReads)
            {
                Assert.True(cr.Rates.Count > 0);
            }
        }

        [Fact]
        public async Task CanGetCurrencyReadWithoutApiKey()
        {
            // The endpoint or route of the controller action.
            var httpResponse = await _client.GetAsync("/api/CurrencyRead?currencyCodes[PLN]=EUR&startDate=2019-09-03&endDate=2019-09-03");

            // Must be unauthorized.
            Assert.Equal(System.Net.HttpStatusCode.Unauthorized, httpResponse.StatusCode);

        }

        [Fact]
        public async Task CanGetCurrencyReadFromDateThatDoesNotExists()
        {
            //check if for date is no data
            var ecbHttpResponse = await _client.GetAsync("https://sdw-wsrest.ecb.europa.eu/service/data/EXR/D.PLN.EUR.SP00.A?startPeriod=2017-09-03&endPeriod=2017-09-03&detail=dataonly");
            Assert.Equal(System.Net.HttpStatusCode.NotFound, ecbHttpResponse.StatusCode);
            var ecbStringResponse = await ecbHttpResponse.Content.ReadAsStringAsync();
            Assert.True(string.IsNullOrEmpty(ecbStringResponse));

            // The endpoint or route of the controller action.
            var httpResponseApi = await _client.GetAsync("/api/CurrencyRead?currencyCodes[PLN]=EUR&startDate=2017-09-03&endDate=2017-09-03&apiKey=TestKeyForExchangeApi");

            // Must be successful.
            httpResponseApi.EnsureSuccessStatusCode();

            // Deserialize and examine results.
            var stringResponse = await httpResponseApi.Content.ReadAsStringAsync();
            var currencyReads = JsonConvert.DeserializeObject<List<CurrencyRead>>(stringResponse);
            Assert.True(currencyReads.Count > 0);
            foreach (var cr in currencyReads)
            {
                Assert.True(cr.Rates.Count > 0);
            }
        }

        [Fact]
        public async Task CanNotGetCurrencyReadWithInvalidDatetime()
        {
            // The endpoint or route of the controller action.
            var httpResponse = await _client.GetAsync("/api/CurrencyRead?currencyCodes[PLN]=EUR&startDate=2019&endDate=2019-09-03&apiKey=TestKeyForExchangeApi");
            Assert.Equal(System.Net.HttpStatusCode.BadRequest, httpResponse.StatusCode);

            httpResponse = await _client.GetAsync("/api/CurrencyRead?currencyCodes[PLN]=EUR&startDate=&endDate=&apiKey=TestKeyForExchangeApi");
            Assert.Equal(System.Net.HttpStatusCode.BadRequest, httpResponse.StatusCode);

            httpResponse = await _client.GetAsync("/api/CurrencyRead?currencyCodes[PLN]=EUR&startDate=2020:03:03&endDate=2019-09-03&apiKey=TestKeyForExchangeApi");
            Assert.Equal(System.Net.HttpStatusCode.BadRequest, httpResponse.StatusCode);

            httpResponse = await _client.GetAsync("/api/CurrencyRead?currencyCodes[PLN]=EUR&apiKey=TestKeyForExchangeApi");
            Assert.Equal(System.Net.HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
    }
}
