﻿using ExchangeRatesAPI.Database;
using ExchangeRatesAPI.Models;

namespace ExchabgeRatesAPI.Test
{
    public static class SeedData
    {
        public static void PopulateTestData(SQLDbContext dbContext)
        {
            dbContext.ApiKeys.Add(new ApiKey("TestKeyForExchangeApi", "TestInstance"));
            dbContext.SaveChanges();
        }
    }
}