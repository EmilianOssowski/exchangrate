# README #

### What is this repository for? ###

ASP.NET Core API project for providing currency rates, using ECB SDMX 2.1 RESTful web service and PostgreSQL database.

### How do I get set up? ###

* Project needs a PostgreSQL server, and before running an API you need to precise connection string in appsettings.json.

### How to use API? ###

Api provides 2 endpoints

First you need to generate your API key, by providing your name in POST request 

* Generate your API key 
- Endpoint: /api/ApiKey
- Type: POST
- Body: 
```
{
	"ownerName" : "<your name>"
}
```
- Response: 
{
	"apiKey" : "<apiKey>"
	"ownerName" : "<your name>"
}

Next you can get data from API

* Get data from API
- Endpoint: /api/CurrencyRead?currencyCodes[<code 1>]=<code 2>&startDate=yyyy-MM-dd&endDate=yyyy-MM-dd&apiKey=<apiKey>
- Type: GET
- Parameters:
	- currencyCodes[<code 1>]=<code 2>, where code 1 is source currency, and code 2 is target currency, both parameters are 3 letter codes of currencies (ex. EUR, PLN, GBP). It is allowed to get in one request more than one combination of curreny codes, providing more than one currencyCodes[<code 1>]=<code 2> parameters. 
	- startDate, which is the beginning of the range of dates in which to search, provided in format yyyy-MM-dd,
	- endDate, which is the end of the range of dates in which to search, provided in format yyyy-MM-dd. If you want to get data from one day it should be the same as startDate parameter.
	- apiKey, which is generated key by the another endpoint in API.
- Response:
```
[
	{
		"currency":"PLN",
		"denominator":"EUR",
		"rates":[
			{
				"startDate":"2019-09-03T00:00:00",
				"endDate":"2019-09-03T23:59:59",
				"value":4.3663
			},
			...
		]
	},
	...
[
			
```
	
	
### Architecture ###

After request firstly API check if for requested range of time there is complete data in local database. 
If data is complete API gets this data from database and send it in response.
If data is not complete API requests ECB REST API for data, and send it back in response, while at the background save this data in local database.
This solution provides more availability and performance of data providing by ECB. It also stays open for integrating with more services which provides currency rates data.
SQL Database contains 3 tables: 
- apiKeys, which stores generated api keys 
- currencyCodes, which stores currency codes of already exisitng rates
- rates, which stores rates for specific days, and foreign key to entity in currencyCodes table


### Technology ###

* ASP.NET Core API
* Entity Framework Core
* AspNetCore.Authentication.ApiKey
* xUnit

* PostgreSQL

### Author ###

* Emilian Ossowski
* Email: lvzik12@gmail.com