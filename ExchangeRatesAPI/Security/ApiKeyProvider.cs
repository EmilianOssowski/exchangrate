﻿using AspNetCore.Authentication.ApiKey;
using ExchangeRatesAPI.Database;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace ExchangeRatesAPI.Security
{
	public class ApiKeyProvider : IApiKeyProvider
	{
		private readonly ILogger<ApiKeyProvider> _logger;
		private readonly SQLDbContext _dbContext;

		public ApiKeyProvider(ILogger<ApiKeyProvider> logger, SQLDbContext dbContext)
		{
			_logger = logger;
			_dbContext = dbContext;
		}

		public Task<IApiKey> ProvideAsync(string key)
		{
			try
			{
				IApiKey apiKey = _dbContext.ApiKeys.Where(k => k.Key == key).FirstOrDefault();
				return Task.FromResult(apiKey);
			}
			catch (System.Exception exception)
			{
				_logger.LogError(exception, exception.Message);
				throw;
			}
		}
	}
}
