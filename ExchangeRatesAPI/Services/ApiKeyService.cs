﻿using ExchangeRatesAPI.Models;
using System;
using System.Security.Cryptography;
using System.Text;

namespace ExchangeRatesAPI.Services
{
    public interface IApiKeyService
    {
        public ApiKey GenerateApiKey(string ownerName);
    }
    public class ApiKeyService : IApiKeyService
    {
        public ApiKey GenerateApiKey(string ownerName)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                var guid = Guid.NewGuid().ToString().Replace("-", "");
                // Convert the input string to a byte array and compute the hash.
                byte[] data = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(ownerName + guid));

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                var sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }
                // Return the hexadecimal string.
                return new ApiKey(sBuilder.ToString(), ownerName);
            }
            
        }
    }
}
