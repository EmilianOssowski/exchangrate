﻿using ExchangeRatesAPI.Database;
using ExchangeRatesAPI.DataTransferObjects;
using ExchangeRatesAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ExchangeRatesAPI.Services
{
    public interface ICurrencyReadService
    {
        public Task<List<CurrencyReadDto>> GetCurrencyReads(Dictionary<string, string> currencyCodes, DateTime startDate, DateTime endDate);
    }
    public class CurrencyReadService : ICurrencyReadService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly ILogger _logger;
        public CurrencyReadService(IServiceScopeFactory scopeFactory, IHttpClientFactory httpClientFactory, ILogger<CurrencyReadService> logger)
        {
            _scopeFactory = scopeFactory;
            _httpClientFactory = httpClientFactory;
            _logger = logger;
        }

        private async Task<CurrencyRead> LoadCurrency(string currency, string denominator, DateTime startDate, DateTime endDate)
        {
            using (var client = _httpClientFactory.CreateClient())
            {
                var currencyRead = new CurrencyRead()
                {
                    Currency = currency,
                    Denominator = denominator,
                };
                //retrive data from ECB
                var request = new HttpRequestMessage(HttpMethod.Get,
                    "https://sdw-wsrest.ecb.europa.eu/service/data/EXR/D." + currency + "." + denominator + ".SP00.A?startPeriod=" + startDate.ToString("yyyy-MM-dd") + "&endPeriod=" + endDate.ToString("yyyy-MM-dd") + "&detail=dataonly");
                request.Headers.Add("Accept", "application/vnd.sdmx.data+json;version=1.0.0-wd");
                request.Headers.Add("User-Agent", "ExchangeRateAPI");
                var response = await client.SendAsync(request);
                var data = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == System.Net.HttpStatusCode.OK && string.IsNullOrEmpty(data) == false)
                {
                    try
                    {
                        //map data to CurrencyRead 
                        var parsed = JObject.Parse(data);
                        var observations = parsed.SelectToken("dataSets").First.SelectToken("series").SelectToken("0:0:0:0:0").SelectToken("observations");
                        var values = parsed.SelectToken("structure").SelectToken("dimensions").SelectToken("observation").First.SelectToken("values");

                        for (int i = 0; i < observations.Children().Count(); i++)
                        {
                            var value = float.Parse(observations.ElementAt(i).First[0].ToString(), System.Globalization.CultureInfo.CurrentCulture);
                            var dateJobject = values.ElementAt(i);
                            var rateStartDate = DateTime.Parse(dateJobject.SelectToken("start").ToString());
                            var rateEndDate = DateTime.Parse(dateJobject.SelectToken("end").ToString());
                            var ecbId = DateTime.Parse(dateJobject.SelectToken("id").ToString()).Ticks;
                            var rate = new Rate()
                            {
                                StartDate = rateStartDate,
                                EndDate = rateEndDate,
                                Value = value,
                                EcbId = ecbId
                            };
                            currencyRead.Rates.Add(rate);
                        }
                        //save data in database async while returning data
                        Task.Run(() => SaveInDb(currencyRead));
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError("Error while parsing data: " + ex);
                    }

                }
                return currencyRead;
            }
        }

        private async Task SaveInDb(CurrencyRead currencyRead)
        {

            using (var scope = _scopeFactory.CreateScope())
            {
                var _dbContext = scope.ServiceProvider.GetService<SQLDbContext>();
                using (var transaction = _dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var existingEntity = _dbContext.CurrencyReads.Where(x => x.Currency == currencyRead.Currency && x.Denominator == currencyRead.Denominator).Include(cr => cr.Rates).FirstOrDefault();
                        // if CurrencyRead not exist create it
                        if (existingEntity == null)
                        {
                            existingEntity = _dbContext.CurrencyReads.Add(currencyRead).Entity;
                            await _dbContext.SaveChangesAsync();
                        }
                        //take every parsed rate and check if exists in db
                        foreach (var rate in currencyRead.Rates)
                        {
                            if (!_dbContext.Rates.Where(r => r.CurrencyReadId == existingEntity.Id && r.EndDate == rate.EndDate && r.StartDate == rate.StartDate).Any())
                            {
                                rate.CurrencyReadId = existingEntity.Id;
                                _dbContext.Rates.Add(rate);
                            }
                        }
                        await _dbContext.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _logger.LogError("Error while saving data in DB: " + ex);
                    }
                }
            }
        }


        public async Task<List<CurrencyReadDto>> GetCurrencyReads(Dictionary<string, string> currencyCodes, DateTime startDate, DateTime endDate)
        {
            //creating list of dates which are we looking
            var dates = new List<long>();

            for (var dt = startDate; dt < endDate; dt = dt.AddDays(1))
            {
                dates.Add(dt.Ticks);
            }
            
            using (var scope = _scopeFactory.CreateScope())
            {
                var _dbContext = scope.ServiceProvider.GetService<SQLDbContext>();

                var result = new List<CurrencyReadDto>();
                                                
                foreach (var key in currencyCodes.Keys)
                {
                    var dto = new CurrencyReadDto()
                    {
                        Currency = key,
                        Denominator = currencyCodes.GetValueOrDefault(key)
                    };
                    //checking if every date from dates variable exists in database
                    var existingDates = _dbContext.Rates
                        .Where(r => r.CurrencyRead.Currency == dto.Currency 
                        && r.CurrencyRead.Denominator == dto.Denominator 
                        && r.StartDate >= startDate 
                        && r.EndDate <= endDate)
                        .Select(s => s.EcbId)
                        .ToList();
                    var isComplete = dates.Intersect(existingDates).Count() == dates.Count();
                    
                    //if lists length are not the same, load data from ECB and save it
                    if (!isComplete)
                    {
                        var emptyDays = 1;
                        CurrencyRead res = new CurrencyRead();
                        //while any result
                        while (res.Rates.Count() == 0)
                        {
                            // limit searching for 90 days before to pretend to search while end of timestamp
                            if(emptyDays == -9)
                            {
                                return null;
                            }
                            emptyDays -= 1;
                            res = await LoadCurrency(dto.Currency, dto.Denominator, startDate.AddDays(emptyDays * 10), endDate);
                        }
                        var rates = new List<RateDto>();
                        rates.Add(res.Rates.Select(s => new RateDto(s)).Last());
                        dto.Rates = rates;
                    }
                    //if lists are the same, load data from database
                    else
                    {
                        var rates = _dbContext.CurrencyReads.Where(cr => cr.Currency == dto.Currency && cr.Denominator == dto.Denominator)
                        .Join(_dbContext.Rates, cr => cr.Id, r => r.CurrencyReadId, (cr, r) => new { CurrencyRead = cr, Rate = r })
                        .Where(r => r.Rate.StartDate >= startDate && r.Rate.EndDate <= endDate)
                        .OrderBy(s => s.Rate.StartDate)
                        .Select(s => new RateDto(s.Rate))
                        .ToList();

                        dto.Rates = rates;
                    }
                    
                    result.Add(dto);
                }
                return result;
            }
        }
    }
}
