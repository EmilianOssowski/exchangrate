﻿using ExchangeRatesAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExchangeRatesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyReadController : ControllerBase
    {
        private readonly ICurrencyReadService _currencyReadService;
        private readonly ILogger _logger;

        public CurrencyReadController(ICurrencyReadService currencyReadService, ILogger<CurrencyReadController> logger)
        {
            _currencyReadService = currencyReadService;
            _logger = logger;
        }

        [Authorize]
        [HttpGet]
        public async Task<JsonResult> Get([FromQuery] Dictionary<string, string> currencyCodes, DateTime startDate, DateTime endDate)
        {
            _logger.LogInformation("GET CurrencyRead from: " + Request.Host.Value + ", with apiKey: " + Request.Query["apiKey"] );
            //dates validation
            if(startDate.Ticks == 0 || endDate.Ticks == 0)
            {
                Response.StatusCode = 400;
                return new JsonResult("Precise startDate and endDate parameters.");
            }
            if(startDate > endDate)
            {
                Response.StatusCode = 400;
                return new JsonResult("Start date must be earlier than the end date.");
            }
            if(startDate > DateTime.Now || endDate > DateTime.Now)
            {
                Response.StatusCode = 404;
                return new JsonResult("No data for this date range.");
            }
            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
            //currency codes validation
            var validatedCurrencyCodes = new Dictionary<string, string>();
            foreach (var key in currencyCodes.Keys)
            {
                var value = currencyCodes.GetValueOrDefault(key);
                if(key.Length != 3 || value.Length != 3)
                {
                    Response.StatusCode = 404;
                    return new JsonResult("Wrong currency codes " + key + ", " + value + ".");
                }
                validatedCurrencyCodes.Add(key.ToUpper(), value.ToUpper());
            }

            var result = await _currencyReadService.GetCurrencyReads(validatedCurrencyCodes, startDate, endDate);
            if (result == null)
            {
                Response.StatusCode = 404;
                return new JsonResult("No data for this startDate and 90 days before, change startDate and try again");
            }

            return new JsonResult(result);
        }

    }
}