﻿using ExchangeRatesAPI.Database;
using ExchangeRatesAPI.Models;
using ExchangeRatesAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;


namespace ExchangeRatesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApiKeyController : ControllerBase
    {
        private readonly SQLDbContext _dbContext;
        private readonly IApiKeyService _apiKeyService;
        private readonly ILogger _logger;

        public ApiKeyController(SQLDbContext dbContext, IApiKeyService apiKeyService, ILogger<ApiKeyController> logger)
        {
            _dbContext = dbContext;
            _apiKeyService = apiKeyService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<JsonResult> Post([FromBody] ApiKey body)
        {
            _logger.LogInformation("POST ApiKey from: " + Request.Host.Value + ", with ownerName: " + body.OwnerName);
            if (string.IsNullOrEmpty(body.OwnerName))
            {
                Response.StatusCode = 400;
                return new JsonResult("No OwnerName specified.");
            }
            var apiKey = _apiKeyService.GenerateApiKey(body.OwnerName);
            _dbContext.ApiKeys.Add(apiKey);
            await _dbContext.SaveChangesAsync();
            return new JsonResult(apiKey);
        }
    }
}
