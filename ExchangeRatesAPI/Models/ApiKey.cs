﻿using AspNetCore.Authentication.ApiKey;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;

namespace ExchangeRatesAPI.Models
{
	public class ApiKey : IApiKey
	{
		public ApiKey() { }
		public ApiKey(string key, string owner, List<Claim> claims = null)
		{
			Key = key;
			OwnerName = owner;
			Claims = claims ?? new List<Claim>();
		}
		public int Id { get; set; }

		public string Key { get; set; }

		public string OwnerName { get; set; }

		[NotMapped]
		public IReadOnlyCollection<Claim> Claims { get; }
	}
}
