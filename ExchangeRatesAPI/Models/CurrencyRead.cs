﻿using System.Collections.Generic;

namespace ExchangeRatesAPI.Models
{
    public class CurrencyRead
    {
        public int Id { get; set; }
        public CurrencyRead()
        {
            Rates = new List<Rate>();
        }
        public string Currency { get; set; }

        public string Denominator { get; set; }
        public List<Rate> Rates { get; set; }
       
    }
}
