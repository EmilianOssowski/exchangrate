﻿using System;

namespace ExchangeRatesAPI.Models
{
    public class Rate
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public float Value { get; set; }
        public int CurrencyReadId { get; set; }
        public CurrencyRead CurrencyRead { get; set; }
        public long EcbId { get; set; }
    }
}
