﻿using System.Collections.Generic;

namespace ExchangeRatesAPI.DataTransferObjects
{
    public class CurrencyReadDto
    {
        public string Currency { get; set; }
        public string Denominator { get; set; }
        public List<RateDto> Rates { get; set; }
    }
}
