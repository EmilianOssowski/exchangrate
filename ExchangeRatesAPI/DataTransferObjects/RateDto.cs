﻿using System;

namespace ExchangeRatesAPI.DataTransferObjects
{
    public class RateDto
    {
        public RateDto(ExchangeRatesAPI.Models.Rate rate)
        {
            StartDate = rate.StartDate;
            EndDate = rate.EndDate;
            Value = rate.Value;
            
        } 
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public float Value { get; set; }
    }
}
