﻿using AspNetCore.Authentication.ApiKey;
using ExchangeRatesAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace ExchangeRatesAPI.Database
{
    public class SQLDbContext : DbContext
    {
        public SQLDbContext(DbContextOptions<SQLDbContext> options) : base(options)
        {

        }
        public DbSet<ApiKey> ApiKeys { get; set; }
        public DbSet<CurrencyRead> CurrencyReads { get; set; }
        public DbSet<Rate> Rates { get; set; }
    }
}
